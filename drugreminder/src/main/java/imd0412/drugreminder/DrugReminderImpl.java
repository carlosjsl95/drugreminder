package imd0412.drugreminder;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class DrugReminderImpl implements IReminder {

	public List<String> createReminders(String startTime, Frequency frequency, Integer duration) {
		
		List<String> timesToTakeRemedy = new ArrayList<String>();
		DecimalFormat df = new DecimalFormat("00");
		
		try {
			DateFormat date = new DateFormat(startTime);
			if (duration > 30 || duration <= 0) throw new IllegalArgumentException("Quantidade de dias acima ou abaixo do permitido");
			
			timesToTakeRemedy.add(startTime);
			Integer totalNumberOfTimesToTakeRemedy = (duration * (24 / frequency.getValue())) -1;    
	        
			for(int i = 0; i < totalNumberOfTimesToTakeRemedy ; i++) {
				date.setHours(date.getHours() + frequency.getValue());
				
				timesToTakeRemedy.add(df.format(date.getDay()).toString()+"/"+
									  df.format(date.getMonth()).toString()+"/"+
									  date.getYear().toString()+" "+
									  df.format(date.getHours()).toString()+":"+
									  df.format(date.getMinutes()).toString());
			}
			
			timesToTakeRemedy.forEach((element) -> System.out.println(element));
			
		} catch (IllegalArgumentException e) {
			System.err.println(e.getMessage());
			throw e;
			//e.printStackTrace();
		}

		return timesToTakeRemedy;
	}
}
