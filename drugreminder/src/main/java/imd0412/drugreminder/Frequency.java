package imd0412.drugreminder;

public enum Frequency {
	SIX_HOURS, EIGHT_HOURS, TWELVE_HOURS, TWENTYFOUR_HOURS;

	public int getValue() {
		switch (this) {
		case SIX_HOURS: return 6;
		case EIGHT_HOURS: return 8;
		case TWELVE_HOURS: return 12;
		case TWENTYFOUR_HOURS: return 24;
		default: return 0;
		}
	}
}
