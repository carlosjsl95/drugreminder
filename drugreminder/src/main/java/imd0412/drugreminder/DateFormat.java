package imd0412.drugreminder;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class DateFormat {

	private Integer day;
	private Integer month;
	private Integer year;
	private Integer totalDaysOfThisMonth;
	private Integer hours;
	private Integer minutes;

	public DateFormat(String startTime)  {
		Map<Integer, Integer> months = new HashMap<Integer, Integer>();

		months.put(1, 31);
		months.put(2, 28);
		months.put(3, 31);
		months.put(4, 30);
		months.put(5, 31);
		months.put(6, 30);
		months.put(7, 31);
		months.put(8, 31);
		months.put(9, 30);
		months.put(10, 31);
		months.put(11, 30);
		months.put(12, 31);
		
		if(!verifyDatePattern(startTime)) throw new IllegalArgumentException("Data inválida");
		
		this.day = Integer.valueOf(startTime.substring(0, 2));
		this.month = Integer.valueOf(startTime.substring(3, 5));
		this.year = Integer.valueOf(startTime.substring(6, 10));
		this.hours = Integer.valueOf(startTime.substring(11, 13));
		this.minutes = Integer.valueOf(startTime.substring(14, 16));
		
		if (isLeapYear(this.year)) months.put(2, 29);
		this.totalDaysOfThisMonth = months.get(month);
		
		if(!verifyDayWithMonth(months)) throw new IllegalArgumentException("Data inválida");
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		if(day > totalDaysOfThisMonth) {
			this.day = 1;
			setMonth(month + 1);
		}else {
			this.day = day;
		}
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		if (month > 12) {
			this.month = 1;
			setYear(this.year + 1);
		}else {
			this.month = month;
		}
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	public Integer getTotalDaysOfThisMonth() {
		return totalDaysOfThisMonth;
	}

	public void setTotalDaysOfThisMonth(Integer totalDaysOfThisMonth) {
		this.totalDaysOfThisMonth = totalDaysOfThisMonth;
	}
	
	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		if(hours >= 24) {
			this.hours = hours - 24;
			setDay(this.day + 1);
		}else {
			this.hours = hours;
		}
	}

	public Integer getMinutes() {
		return minutes;
	}

	public void setMinutes(Integer minutes) {
		this.minutes = minutes;
	}
	
	public boolean verifyDatePattern(String startTime) {
		String regex = "^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4} ([0-1]\\d|2[0-3]):([0-5]\\d)$";
		if(!Pattern.matches(regex, startTime)) return false;
		return true;
	}
	
	public boolean isLeapYear(Integer year) {
		if ( ( year % 4 == 0 && year % 100 != 0 ) || ( year % 400 == 0 ) ) return true;
		return false;
	}
	
	public boolean verifyDayWithMonth(Map<Integer, Integer> months) {
		if (day > totalDaysOfThisMonth || year < 2018) return false;
		return true;
	}
	
}
