package imd0412.drugreminder;

import static imd0412.drugreminder.Frequency.*;

public class Main {

	public static void main(String[] args) {
		DrugReminderImpl drug = new DrugReminderImpl();
		drug.createReminders("31/12/2018 00:00", TWENTYFOUR_HOURS, 7);
	}
}
