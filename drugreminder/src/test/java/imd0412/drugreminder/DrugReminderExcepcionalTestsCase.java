package imd0412.drugreminder;

import static imd0412.drugreminder.Frequency.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class DrugReminderExcepcionalTestsCase {

	DrugReminderImpl reminder = new DrugReminderImpl();

	@Parameters(name = "{3}")
	public static Collection input() {
		return Arrays.asList(new Object[][] { 
								{
									"25/10/2018 00:00",SIX_HOURS,0,"Duração igual a 0"
								},
								{
									"30/11/2018 00:20",TWELVE_HOURS,31,"Duração maior que 30"
								},
								{
									"2x/08/2018 10:00",EIGHT_HOURS,2,"Data fora do padrão proposto"
								},
								{
									"32/06/2024 11:20",SIX_HOURS,4,"Extrapolar o limite de dias maximo de um mes"
								},
								{
									"18/06/2014 11:20",SIX_HOURS,2,"Ano menor que 2018"
								},
								{
									"15/17/2014 11:20",TWELVE_HOURS,5,"Extrapolar a quantidade de meses em um ano"
								},
								{
									"04/08/2014 34:20",TWELVE_HOURS,6,"Extrapolar a quantidade de horas maxima em um dia"
								},
								{
									"04/08/2014 14:60",TWELVE_HOURS,6,"Extrapolar a quantidade de segundos maximo em um minuto"
								},
							});
	}

	@Parameter(0)
	public String startTime;

	@Parameter(1)
	public Frequency frequency;

	@Parameter(2)
	public Integer duration;

	@Parameter(3)
	public String description;

	@Test
	(expected = IllegalArgumentException.class)
	public void testDrugReminder() {
		reminder.createReminders(startTime, frequency, duration);
	}
}
