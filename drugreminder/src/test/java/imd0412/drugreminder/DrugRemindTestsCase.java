package imd0412.drugreminder;

import static imd0412.drugreminder.Frequency.*;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class DrugRemindTestsCase {
	
	DrugReminderImpl reminder = new DrugReminderImpl();
	
	
	@Parameters( name = "{4}" )
	public static Collection input() {
		return Arrays.asList(new Object [][] {
			{
				"30/12/2018 00:00",TWENTYFOUR_HOURS,3,Arrays.asList("30/12/2018 00:00", "31/12/2018 00:00", "01/01/2019 00:00"),"Verificar virada de ano"
			},
			{
				"27/02/2020 00:00",TWENTYFOUR_HOURS,4,Arrays.asList("27/02/2020 00:00", "28/02/2020 00:00", "29/02/2020 00:00", "01/03/2020 00:00"),"Verificar se possui suporte para ano bissexto"
			}
		});
	}
	
	@Parameter(0)
	public String startTime;
	
	@Parameter(1)
	public Frequency frequency;
	
	@Parameter(2)
	public Integer duration;
	
	@Parameter(3)
	public List<String> remindersList;
	
	@Parameter(4)
	public String description;
	
	@Test
	public void testDrugReminder()  {	
		assertEquals(remindersList,reminder.createReminders(startTime, frequency, duration));
	}
}
