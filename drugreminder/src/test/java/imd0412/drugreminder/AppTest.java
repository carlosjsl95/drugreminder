package imd0412.drugreminder;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Unit test for simple App.
 */
@RunWith(Suite.class)

@Suite.SuiteClasses({ DrugRemindTestsCase.class, DrugReminderExcepcionalTestsCase.class })
public class AppTest {

}
